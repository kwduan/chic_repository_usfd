from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns('',
    url(r'^model_repository/', include('model_repository.urls')),
    url(r'^metadata_repository/', include('metadata_repository.urls')),
    url(r'^admin/', include(admin.site.urls)),
    # Examples:
    # url(r'^$', 'repository.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
)
