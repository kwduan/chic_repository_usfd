__author__ = 'Kewei Duan'

from django.conf.urls import patterns, url

from . import views

urlpatterns = patterns('',
                       url(r'^metadata/(?P<metadata_unique_name>\w+)/$', views.JsonView.as_view()),
                       url(r'^metadata/$', views.ListView.as_view()),
                       )
