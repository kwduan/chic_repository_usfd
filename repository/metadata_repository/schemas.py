__author__ = 'Kewei Duan'

chic_schema = {
    "$schema": "http://json-schema.org/draft-04/schema#",
    "id": "http://chic-schema.com",
    "title": "CHIC resource",
    "description": "A metadata schema for CHIC resources",
    "type": "object",
    "properties": {
        "ResourceID": {
            "id": "http://chic-schema.com/ResourceID",
            "description": "Root of the metadata",
            "type": "string",
            "properties": {
                "SchemaURI": {
                    "id": "http://chic-schema.com/ResourceID/SchemaURI",
                    "description": "URI to the schema used to annotate the resource",
                    "type": "string",
                    "format": "uri"
                },
                "SchemaVersion": {
                    "id": "http://chic-schema.com/ResourceID/SchemaVersion",
                    "description": "date of the version of the used schema",
                    "type": "string",
                    "format": "date-time"
                },
                "CommonMetadata": {
                    "id": "http://chic-schema.com/ResourceID/CommonMetadata",
                    "description": "metadata common to all resource types",
                    "type": "object",
                    "properties": {
                        "GlobalID": {
                            "id": "http://chic-schema.com/ResourceID/CommonMetadata/GlobalID",
                            "description": "unique globalID of the resource on the CHIC infrastructure",
                            "type": "string",
                            "format": "uri"
                        },
                        "LocalID": {
                            "id": "http://chic-schema.com/ResourceID/CommonMetadata/LocalID",
                            "description": "ID internal to the resource provider",
                            "type": "string"
                        },
                        "Name": {
                            "id": "http://chic-schema.com/ResourceID/CommonMetadata/Name",
                            "description": "title given by the author to the resource",
                            "type": "string"
                        },
                        "Type": {
                            "id": "http://chic-schema.com/ResourceID/CommonMetadata/Type",
                            "description": "type of resource",
                            "enum": [
                                "dataset",
                                "hypomodel",
                                "hypermodel"
                            ]
                        },
                        "ResourceDescription": {
                            "id": "http://chic-schema.com/ResourceID/CommonMetadata/ResourceDescription",
                            "description": "textual human readable description of the resource",
                            "type": "string"
                        },
                        "Tags": {
                            "id": "http://chic-schema.com/ResourceID/CommonMetadata/Tags",
                            "description": "tags to be added by the users for folksonomies",
                            "type": "array"
                        },
                        "ResourceAuthor": {
                            "id": "http://chic-schema.com/ResourceID/CommonMetadata/ResourceAuthor",
                            "description": "creator of the resource",
                            "type": "array"
                        },
                        "Access": {
                            "id": "http://chic-schema.com/ResourceID/CommonMetadata/Access",
                            "description": "level of visibility of the resource",
                            "enum": [
                                "private",
                                "sharedAmongPrivateGroups",
                                "publicAccess"
                            ]
                        },
                        "SemanticRepresentation": {
                            "id": "http://chic-schema.com/ResourceID/CommonMetadata/SemanticRepresentation",
                            "description": "semantic annotation sub-tags",
                            "type": "string",
                            "format": "uri"
                        },
                        "Quality": {
                            "id": "http://chic-schema.com/ResourceID/CommonMetadata/Quality",
                            "description": "Quality indicators",
                            "type": "object",
                            "properties": {
                                "CurationState": {
                                    "id": "http://chic-schema.com/ResourceID/CommonMetadata/Quality/CurationState",
                                    "description": "Represents the state of curation for the object",
                                    "enum": [
                                        "Ingested",
                                        "Annotated",
                                        "Curated"
                                    ]
                                },
                                "Rating": {
                                    "id": "http://chic-schema.com/ResourceID/CommonMetadata/Quality/Rating",
                                    "description": "average rating of the resource by users",
                                    "type": "number"
                                },
                                "Views": {
                                    "id": "http://chic-schema.com/ResourceID/CommonMetadata/Quality/Views",
                                    "description": "number of accesses/views of the resource",
                                    "type": "integer"
                                }
                            }
                        },
                        "Documentation": {
                            "id": "http://chic-schema.com/ResourceID/CommonMetadata/Documentation",
                            "description": "Documentation sub-section",
                            "type": "object",
                            "properties": {
                                "ResourceCopyright": {
                                    "id": "http://chic-schema.com/ResourceID/CommonMetadata/Documentation/ResourceCopyright",
                                    "description": "copyright of the resource",
                                    "type": "string"
                                },
                                "LicenseType": {
                                    "id": "http://chic-schema.com/ResourceID/CommonMetadata/Documentation/LicenseType",
                                    "description": "type of license associated to the resource",
                                    "enum": [
                                        "GPL",
                                        "LGPL",
                                        "EULA",
                                        "OLP",
                                        "TLP",
                                        "VLP",
                                        "BSD",
                                        "MIT",
                                        "proprietary",
                                        "CreativeCommons"
                                    ]
                                },
                                "LicenseURL": {
                                    "id": "http://chic-schema.com/ResourceID/CommonMetadata/Documentation/LicenseURL",
                                    "description": "file or url description of the license",
                                    "type": "string",
                                    "format": "uri"
                                },
                                "Reference": {
                                    "id": "http://chic-schema.com/ResourceID/CommonMetadata/Documentation/Reference",
                                    "description": "N/A",
                                    "type": "object",
                                    "properties": {
                                        "Type": {
                                            "id": "http://chic-schema.com/ResourceID/CommonMetadata/Documentation/Reference/Type",
                                            "description": "N/A",
                                            "enum": ["book",
                                                     "journal",
                                                     "article",
                                                     "conferenceArticle",
                                                     "other"]
                                        },
                                        "DOI": {
                                            "id": "http://chic-schema.com/ResourceID/CommonMetadata/Documentation/Reference/DOI",
                                            "description": "N/A",
                                            "type": "string"
                                        },
                                        "PubMedIdentifier": {
                                            "id": "http://chic-schema.com/ResourceID/CommonMetadata/Documentation/Reference/PubMedIdentifier",
                                            "description": "N/A",
                                            "type": "string"
                                        },
                                        "DateOfFormalIssuance": {
                                            "id": "http://chic-schema.com/ResourceID/CommonMetadata/Documentation/Reference/DateOfFormalIssuance",
                                            "description": "N/A",
                                            "type": "date-time"
                                        },
                                        "BibliographicCitation": {
                                            "id": "http://chic-schema.com/ResourceID/CommonMetadata/Documentation/Reference/BibliographicCitation",
                                            "description": "N/A",
                                            "type": "array"
                                        },
                                        "OriginResource": {
                                            "id": "http://chic-schema.com/ResourceID/CommonMetadata/Documentation/Reference/OriginResource",
                                            "description": "N/A",
                                            "type": "string"
                                        }
                                    }
                                }
                            },
                            "required": [
                                "ResourceCopyright",
                                "LicenseType"
                            ]
                        },
                        "Provenance": {
                            "id": "http://chic-schema.com/ResourceID/CommonMetadata/Provenance",
                            "description": "traceability sub-section; one for each event",
                            "type": "object",
                            "properties": {
                                "Event": {
                                    "id": "http://chic-schema.com/ResourceID/CommonMetadata/Provenance/Event",
                                    "description": "list of events to be extended (create mandatory as a minimum)",
                                    "enum": [
                                        "create",
                                        "update",
                                        "delete"
                                    ]
                                },
                                "EventDate": {
                                    "id": "http://chic-schema.com/ResourceID/CommonMetadata/Provenance/EventDate",
                                    "description": "date of event",
                                    "type": "string",
                                    "format": "date-time"
                                },
                                "OperatorID": {
                                    "id": "http://chic-schema.com/ResourceID/CommonMetadata/Provenance/OperatorID",
                                    "description": "user performing the action on the resource",
                                    "type": "string"
                                },
                                "Parameters": {
                                    "id": "http://chic-schema.com/ResourceID/CommonMetadata/Provenance/Parameters",
                                    "description": "parameters eventually associated with the event",
                                    "type": "array"
                                },
                                "Version": {
                                    "id": "http://chic-schema.com/ResourceID/CommonMetadata/Provenance/Version",
                                    "description": "Identficator of the resource version",
                                    "type": "array"
                                }
                            }
                        },
                        "PrimaryCharacterisation": {
                            "id": "http://chic-schema.com/ResourceID/CommonMetadata/PrimaryCharacterisation",
                            "description": "Characterisation perspectives as from D6.1",
                            "type": "object",
                            "properties": {
                                "Perspective1": {
                                    "id": "http://chic-schema.com/ResourceID/CommonMetadata/PrimaryCharacterisation/Perspective1",
                                    "description": "Tumor-affected/normal tissue modelling",
                                    "enum": [
                                        "tumour",
                                        "normal",
                                        "treatment_response"
                                    ]
                                },
                                "Perspective2": {
                                    "id": "http://chic-schema.com/ResourceID/CommonMetadata/PrimaryCharacterisation/Perspective2",
                                    "description": "Spatial scales of manifestation of life ",
                                    "enum": [
                                        "molecule",
                                        "cell",
                                        "tissue",
                                        "organ",
                                        "organism"
                                    ]
                                },
                                "Perspective3": {
                                    "id": "http://chic-schema.com/ResourceID/CommonMetadata/PrimaryCharacterisation/Perspective3",
                                    "description": "Temporal scale of the manifestation of life",
                                    "enum": [
                                        "nsec",
                                        "msec",
                                        "sec",
                                        "min",
                                        "h",
                                        "d",
                                        "y"
                                    ]
                                },
                                "Perspective4": {
                                    "id": "http://chic-schema.com/ResourceID/CommonMetadata/PrimaryCharacterisation/Perspective4",
                                    "description": "biomechanisms addressed",
                                    "type": "array"
                                },
                                "Perspective5": {
                                    "id": "http://chic-schema.com/ResourceID/CommonMetadata/PrimaryCharacterisation/Perspective5",
                                    "description": "tumor types addressed",
                                    "type": "array"
                                },
                                "Perspective6": {
                                    "id": "http://chic-schema.com/ResourceID/CommonMetadata/PrimaryCharacterisation/Perspective6",
                                    "description": "treatment modalities addressed",
                                    "type": "array"
                                }
                            }
                        }
                    },
                    "required": [
                        "GlobalID",
                        "Type",
                        "ResourceAuthor",
                        "Access"
                    ]
                },
                "SpecificMetaData": {
                    "id": "http://chic-schema.com/ResourceID/SpecificMetaData",
                    "description": "information specific for each resource type",
                    "type": "object",
                    "properties": {
                        "DataFile": {
                            "id": "http://chic-schema.com/ResourceID/SpecificMetaData/DataFile",
                            "description": "Data resource stored in a single file",
                            "type": "object",
                            "properties": {
                                "StorageURI": {
                                    "id": "http://chic-schema.com/ResourceID/SpecificMetaData/DataFile/StorageURI",
                                    "description": "where the data file is located",
                                    "type": "string",
                                    "format": "uri"
                                },
                                "FileSize": {
                                    "id": "http://chic-schema.com/ResourceID/SpecificMetaData/DataFile/FileSize",
                                    "description": "dimension of the file",
                                    "type": "integer"
                                },
                                "FileCheckSum": {
                                    "id": "http://chic-schema.com/ResourceID/SpecificMetaData/DataFile/FileCheckSum",
                                    "description": "checksum to be used to control upload/donwload integrity",
                                    "type": "string"
                                },
                                "FileFormat": {
                                    "id": "http://chic-schema.com/ResourceID/SpecificMetaData/DataFile/FileFormat",
                                    "description": "N/A",
                                    "enum": [
                                        "DICOM",
                                        "zip",
                                        "rar",
                                        "jpg",
                                        "tiff",
                                        "raw",
                                        "csv",
                                        "other"
                                    ]
                                },
                                "FileType": {
                                    "id": "http://chic-schema.com/ResourceID/SpecificMetaData/DataFile/FileType",
                                    "description": "N/A",
                                    "enum": [
                                        "ascii",
                                        "binary",
                                        "other"
                                    ]
                                },
                                "Endianity": {
                                    "id": "http://chic-schema.com/ResourceID/SpecificMetaData/DataFile/Endianity",
                                    "description": "N/A",
                                    "enum": [
                                        "little",
                                        "big"
                                    ]
                                },
                                "Encryption": {
                                    "id": "http://chic-schema.com/ResourceID/SpecificMetaData/DataFile/Encryption",
                                    "description": "N/A",
                                    "type": "boolean"
                                }
                            }
                        },
                        "Dataset": {
                            "id": "http://chic-schema.com/ResourceID/SpecificMetaData/Dataset",
                            "description": "data subsection",
                            "type": "object",
                            "properties": {
                                "StorageURI": {
                                    "id": "http://chic-schema.com/ResourceID/SpecificMetaData/Dataset/StorageURI",
                                    "description": "where the data file is located",
                                    "type": "array",
                                    "format": "uri"
                                },
                                "SetURI": {
                                    "id": "http://chic-schema.com/ResourceID/SpecificMetaData/Dataset/SetURI",
                                    "description": "where the set (as zipped directory) is located",
                                    "type": "array",
                                    "format": "uri"
                                },
                                "Encryption": {
                                    "id": "http://chic-schema.com/ResourceID/SpecificMetaData/Dataset/Encryption",
                                    "description": "N/A",
                                    "type": "boolean"
                                }
                            },
                            "required": [
                                "StorageURI",
                                "FileSize",
                                "FileCheckSum",
                                "FileFormat",
                                "FileType",
                                "Endianity",
                                "Encryption"
                            ]
                        },
                        "Hypomodel": {
                            "id": "http://chic-schema.com/ResourceID/SpecificMetaData/Hypomodel",
                            "description": "models subsection",
                            "type": "object",
                            "properties": {
                                "SecondaryCharacterisation": {
                                    "id": "http://chic-schema.com/ResourceID/SpecificMetaData/Hypomodel/SecondaryCharacterisation",
                                    "description": "N/A",
                                    "type": "object",
                                    "properties": {
                                        "Perspective7": {
                                            "id": "http://chic-schema.com/ResourceID/SpecificMetaData/Hypomodel/SecondaryCharacterisation/Perspective7",
                                            "description": "generic cancer biology / clinically driven character of modelling",
                                            "type": "string"
                                        },
                                        "Perspective10": {
                                            "id": "http://chic-schema.com/ResourceID/SpecificMetaData/Hypomodel/SecondaryCharacterisation/Perspective10",
                                            "description": "mechanistic-statistical character of the modelling approach",
                                            "type": "string"
                                        },
                                        "Perspective11": {
                                            "id": "http://chic-schema.com/ResourceID/SpecificMetaData/Hypomodel/SecondaryCharacterisation/Perspective11",
                                            "description": "deterministic-stochastic character of the modelling approach",
                                            "type": "string"
                                        },
                                        "Perspective12": {
                                            "id": "http://chic-schema.com/ResourceID/SpecificMetaData/Hypomodel/SecondaryCharacterisation/Perspective12",
                                            "description": "continuous, finite, discrete character of the mathematics involved",
                                            "type": "string"
                                        },
                                        "Perspective13": {
                                            "id": "http://chic-schema.com/ResourceID/SpecificMetaData/Hypomodel/SecondaryCharacterisation/Perspective13",
                                            "description": "closed form solution or algorithmic",
                                            "type": "string"
                                        }
                                    }
                                },
                                "Inputs": {
                                    "id": "http://chic-schema.com/ResourceID/SpecificMetaData/Hypomodel/Inputs",
                                    "description": "inputs to the model",
                                    "type": "array"
                                },
                                "Outputs": {
                                    "id": "http://chic-schema.com/ResourceID/SpecificMetaData/Hypomodel/Outputs",
                                    "description": "outputs to the model",
                                    "type": "array"
                                },
                                "ModelType": {
                                    "id": "http://chic-schema.com/ResourceID/SpecificMetaData/Hypomodel/ModelType",
                                    "description": "three type of models considered according to the generic stub definition - fields are not individually mandatory but at least one must be selected.",
                                    "type": "string"
                                },
                                "HypomodelURI": {
                                    "id": "http://chic-schema.com/ResourceID/SpecificMetaData/Hypomodel/HypomodelURI",
                                    "description": "where the hypomodel can be phyiscally accessed",
                                    "type": "string",
                                    "format": "uri",
                                    "properties": {
                                        "Static": {
                                            "id": "http://chic-schema.com/ResourceID/SpecificMetaData/Hypomodel/HypomodelURI/Static",
                                            "description": "webservice-like subsection",
                                            "type": "object",
                                            "properties": {
                                                "ProtocolType": {
                                                    "id": "http://chic-schema.com/ResourceID/SpecificMetaData/Hypomodel/HypomodelURI/Static/ProtocolType",
                                                    "description": "protocol to be used for the communication",
                                                    "enum": [
                                                        "REST",
                                                        "SOAP",
                                                        "xmlrpc"
                                                    ]
                                                },
                                                "EndPoint": {
                                                    "id": "http://chic-schema.com/ResourceID/SpecificMetaData/Hypomodel/HypomodelURI/Static/EndPoint",
                                                    "description": "service endpoint",
                                                    "type": "string",
                                                    "format": "uri"
                                                }
                                            }
                                        },
                                        "Migrating": {
                                            "id": "http://chic-schema.com/ResourceID/SpecificMetaData/Hypomodel/HypomodelURI/Migrating",
                                            "description": "virtual machines like sub-section",
                                            "type": "object",
                                            "properties": {
                                                "TargetPort": {
                                                    "id": "http://chic-schema.com/ResourceID/SpecificMetaData/Hypomodel/HypomodelURI/Migrating/TargetPort",
                                                    "description": "port number at which the model anwers",
                                                    "type": "integer"
                                                },
                                                "TransportProtocol": {
                                                    "id": "http://chic-schema.com/ResourceID/SpecificMetaData/Hypomodel/HypomodelURI/Migrating/TransportProtocol",
                                                    "description": "protocol to be used for the communication",
                                                    "enum": [
                                                        "TCP",
                                                        "UDP",
                                                        "other"
                                                    ]
                                                },
                                                "ApplicationProtocol": {
                                                    "id": "http://chic-schema.com/ResourceID/SpecificMetaData/Hypomodel/HypomodelURI/Migrating/ApplicationProtocol",
                                                    "description": "list to be extended",
                                                    "enum": [
                                                        "HTTP",
                                                        "HTTPS",
                                                        "FTP",
                                                        "SSH"
                                                    ]
                                                }
                                            }
                                        },
                                        "Configurable": {
                                            "id": "http://chic-schema.com/ResourceID/SpecificMetaData/Hypomodel/HypomodelURI/Configurable",
                                            "description": "script/executables like subsection",
                                            "type": "object",
                                            "properties": {
                                                "OS": {
                                                    "id": "http://chic-schema.com/ResourceID/SpecificMetaData/Hypomodel/HypomodelURI/Configurable/OS",
                                                    "description": "N/A",
                                                    "enum": [
                                                        "Windows",
                                                        "Linux",
                                                        "Mac"
                                                    ]
                                                },
                                                "Environment": {
                                                    "id": "http://chic-schema.com/ResourceID/SpecificMetaData/Hypomodel/HypomodelURI/Configurable/Environment",
                                                    "description": "N/A",
                                                    "enum": [
                                                        "Matlab",
                                                        "C",
                                                        "C++",
                                                        "Python",
                                                        "JAVA"
                                                    ]
                                                },
                                                "DependingLibraries": {
                                                    "id": "http://chic-schema.com/ResourceID/SpecificMetaData/Hypomodel/HypomodelURI/Configurable/DependingLibraries",
                                                    "description": "libraries needed to be installed for the model to run",
                                                    "type": "array"
                                                }
                                            }
                                        }
                                    }
                                },
                                "BenchmarkRuntime": {
                                    "id": "http://chic-schema.com/ResourceID/SpecificMetaData/Hypomodel/BenchmarkRuntime",
                                    "description": "Typical execution time",
                                    "type": "string"
                                },
                                "BenchmarkInput": {
                                    "id": "http://chic-schema.com/ResourceID/SpecificMetaData/Hypomodel/BenchmarkInput",
                                    "description": "Input fot benchmark test",
                                    "type": "array",
                                    "format": "uri"
                                },
                                "AuditLog": {
                                    "id": "http://chic-schema.com/ResourceID/SpecificMetaData/Hypomodel/AuditLog",
                                    "description": "link to the audit log",
                                    "type": "string",
                                    "format": "uri"
                                }
                            },
                            "required": [
                                "Inputs",
                                "Outputs",
                                "ModelType",
                                "HypomodelURI"
                            ]
                        },
                        "Hypermodel": {
                            "id": "http://chic-schema.com/ResourceID/SpecificMetaData/Hypermodel",
                            "description": "hypermodel subsection",
                            "type": "object",
                            "properties": {
                                "inputs": {
                                    "id": "http://chic-schema.com/ResourceID/SpecificMetaData/Hypermodel/inputs",
                                    "description": "inputs to the model",
                                    "type": "array"
                                },
                                "outputs": {
                                    "id": "http://chic-schema.com/ResourceID/SpecificMetaData/Hypermodel/outputs",
                                    "description": "outputs to the model",
                                    "type": "array"
                                },
                                "HypermodelURI": {
                                    "id": "http://chic-schema.com/ResourceID/SpecificMetaData/Hypermodel/HypermodelURI",
                                    "description": "where the hypermodel description file is located",
                                    "type": "string",
                                    "format": "uri"
                                },
                                "TertiaryCharacterisation": {
                                    "id": "http://chic-schema.com/ResourceID/SpecificMetaData/Hypermodel/TertiaryCharacterisation",
                                    "description": "N/A",
                                    "type": "object",
                                    "properties": {
                                        "Perspective8": {
                                            "id": "http://chic-schema.com/ResourceID/SpecificMetaData/Hypermodel/TertiaryCharacterisation/Perspective8",
                                            "description": "order of addressing different spatial scales",
                                            "enum": [
                                                "bottom_up",
                                                "top_down",
                                                "middle_out"
                                            ]
                                        },
                                        "Perspective9": {
                                            "id": "http://chic-schema.com/ResourceID/SpecificMetaData/Hypermodel/TertiaryCharacterisation/Perspective9",
                                            "description": "order of addressing different temporal scales",
                                            "enum": [
                                                "bottom_up",
                                                "top_down",
                                                "middle_out"
                                            ]
                                        }
                                    }
                                },
                                "BenchmarkRuntime": {
                                    "id": "http://chic-schema.com/ResourceID/SpecificMetaData/Hypermodel/BenchmarkRuntime",
                                    "description": "Typical execution time",
                                    "type": "string"
                                },
                                "BenchmarkInput": {
                                    "id": "http://chic-schema.com/ResourceID/SpecificMetaData/Hypermodel/BenchmarkInput",
                                    "description": "Input fot benchmark test",
                                    "type": "string",
                                    "format": "uri"
                                },
                                "AuditLog": {
                                    "id": "http://chic-schema.com/ResourceID/SpecificMetaData/Hypermodel/AuditLog",
                                    "description": "link to the audit log",
                                    "type": "string",
                                    "format": "uri"
                                }
                            },
                            "required": [
                                "inputs",
                                "outputs",
                                "HypermodelURI"
                            ]
                        },
                        "Trial": {
                            "id": "http://chic-schema.com/ResourceID/SpecificMetaData/Trial",
                            "description": "Clinical trial subsection",
                            "type": "object",
                            "properties": {
                                "Experiment": {
                                    "id": "http://chic-schema.com/ResourceID/SpecificMetaData/Trial/Experiment",
                                    "description": "Experiment Subsection",
                                    "type": "object",
                                    "properties": {
                                        "ExperimentDescription": {
                                            "id": "http://chic-schema.com/ResourceID/SpecificMetaData/Trial/Experiment/ExperimentDescription",
                                            "description": "N/A",
                                            "type": "string"
                                        },
                                        "ExperimentSubject": {
                                            "id": "http://chic-schema.com/ResourceID/SpecificMetaData/Trial/Experiment/ExperimentSubject",
                                            "description": "N/A",
                                            "type": "object",
                                            "maxItems": "2",
                                            "properties": {
                                                "ExternalID": {
                                                    "id": "http://chic-schema.com/ResourceID/SpecificMetaData/Trial/Experiment/ExperimentSubject/ExternalID",
                                                    "description": "Subject ID from external repository",
                                                    "type": "string"
                                                },
                                                "ExternalURI": {
                                                    "id": "http://chic-schema.com/ResourceID/SpecificMetaData/Trial/Experiment/ExperimentSubject/ExternalURI",
                                                    "description": "Subject URI from external repository",
                                                    "type": "string",
                                                    "format": "uri"
                                                }
                                            }
                                        }
                                    }
                                },
                                "TrialDescription": {
                                    "id": "http://chic-schema.com/ResourceID/SpecificMetaData/Trial/TrialDescription",
                                    "description": "N/A",
                                    "type": "string"
                                },
                                "HypermodelURI": {
                                    "id": "http://chic-schema.com/ResourceID/SpecificMetaData/Trial/HypermodelURI",
                                    "description": "N/A",
                                    "type": "string",
                                    "format": "uri"
                                }
                            }
                        }
                    }
                },
                "required": [
                    "SchemaURI",
                    "SchemaVersion",
                    "CommonMetadata",
                    "SpecificMetaData"
                ]
            },
            "required": [
                "ResourceID"
            ]
        }
    }
}