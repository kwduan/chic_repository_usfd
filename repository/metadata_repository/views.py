__author__ = 'Kewei Duan'

from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser
from django.http import HttpResponse

from rest_framework.exceptions import ParseError
from rest_framework.response import Response
from rest_framework import views
from rest_framework import status

from . import schemas, utils
import jsonschema
import json
from jsonschema import ValidationError

class JsonView(views.APIView):
    """
    The APIView used to do CRUD operations on individual json file
    """
    renderer_classes = (JSONRenderer, )
    parser_classes = {JSONParser, }

    def get(self, request, metadata_unique_name):
        """The GET method

        Keyword arguments:
        request -- the HTTP request
        metadata_unique_name -- the ID of the metadata json file
        """
        try:
            return Response(json.loads(utils.retrieve_the_json(json_name=metadata_unique_name)))
        except IOError:
            return Response(status.HTTP_404_NOT_FOUND)

    def post(self, request, metadata_unique_name):
        """The POST method

        Keyword arguments:
        request -- the HTTP request
        metadata_unique_name -- the ID of the metadata json file
        """
        try:
            if utils.if_the_json_exists(metadata_unique_name):
                return Response(status.HTTP_409_CONFLICT)
            else:
                request_body = json.dumps(request.data)
                # jsonschema.validate(request_body, schemas.chic_schema)
                #this line works for json schema validation
                utils.store_the_json(json_data=request_body, json_name=metadata_unique_name)
                return Response(status.HTTP_201_CREATED)
        except ValidationError as error:
            return Response(
                'Invalid JSON - {0}'.format(error.message),
                status=status.HTTP_400_BAD_REQUEST
            )

    def put(self, request, metadata_unique_name):
        """The PUT method

        Keyword arguments:
        request -- the HTTP request
        metadata_unique_name -- the ID of the metadata json file
        """
        try:
            request_body = json.dumps(request.data)
            # jsonschema.validate(request_body, schemas.chic_schema)
            #this line works for json schema validation
            utils.store_the_json(json_data=request_body, json_name=metadata_unique_name)
            return Response(status.HTTP_201_CREATED)
        except ValidationError as error:
            return Response(
                'Invalid JSON - {0}'.format(error.message),
                status=status.HTTP_400_BAD_REQUEST
            )

    def delete(self, metadata_unique_name):
        """The DELETE method

        Keyword arguments:
        metadata_unique_name -- the ID of the metadata json file
        """
        try:
            utils.delete_the_json(metadata_unique_name)
            return Response(status.HTTP_204_NO_CONTENT)
        except:
            return Response(status.HTTP_500_INTERNAL_SERVER_ERROR)


class ListView(views.APIView):
    """
    The APIView used to return a list of all existing json files
    """

    def get(self, request):
        """The GET method

        Keyword arguments:
        request -- the HTTP request
        """
        try:
            json_list = utils.list_all_the_json_files()
            return Response(json_list, status=status.HTTP_200_OK)
        except IOError:
            return Response(status.HTTP_404_NOT_FOUND)
