__author__ = 'Kewei Duan'

import os.path
from os import listdir
from os.path import isfile, join

this_dir = os.path.dirname(__file__)
save_path = os.path.join(this_dir, 'json_folder/')


def if_the_json_exists(json_name):
    """Check if the json file exists in the folder

    :param json_name(String): The ID of the metadata json
    :return: files list(List)
    """
    filename = os.path.join(save_path, json_name + '.json')
    return os.path.isfile(filename)


def store_the_json(json_data, json_name):
    """Store the json file to file system

    :param json_data: The json string
    :param json_name: The ID of the metadata json
    :return:
    """
    try:
        filename = os.path.join(save_path, json_name + '.json')
        f = open(filename, 'w')
        f.write(json_data)
        return
    except IOError:
        raise


def retrieve_the_json(json_name):
    """Retrieve the json file from the file system

    :param json_name: The ID of the metadata json
    :return: The json string
    """
    try:
        filename = os.path.join(save_path, json_name + '.json')
        f = open(filename, 'r')
        return f.read()
    except IOError:
        raise


def delete_the_json(json_name):
    """Retrieve the json file from the file system

    :param json_name: The ID of the metadata json
    :return:
    """
    try:
        filename = os.path.join(save_path, json_name + '.json')
        os.remove(filename)
        return
    except IOError:
        raise


def list_all_the_json_files():
    """List all the json files in the folder

    :return:List
    """
    try:
        only_files = [f for f in listdir(save_path) if isfile(join(save_path, f))]
        return only_files
    except IOError:
        raise
