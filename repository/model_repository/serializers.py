__author__ = 'Daniele Tartarini'

from django.forms import widgets
from rest_framework import serializers
from rest_framework.renderers import JSONRenderer
#from rest_framework.renderers import XMLRenderer
from rest_framework.parsers import JSONParser
#from rest_framework.parsers import XMLParser

from .models import ChicModel


#############################################################
# implementation using serializers.Serializers

"""
class ChicModelSerializer(serializers.Serializer):
    model_unique_name  = serializers.CharField(required=True, allow_blank=True, max_length=100)     # change name
    model_url          = serializers.CharField(required=True, allow_blank=True, max_length=200)
    pk = serializers.IntegerField(read_only=True)
    model_unique_id    = serializers.IntegerField(read_only=True)
    publication_date   = serializers.DateField() # change name
    modeller_name      = serializers.CharField(required=False, allow_blank=True, max_length=200)
    institution  = serializers.CharField(required=False, allow_blank=True, max_length=200)
    title        = serializers.CharField(required=False, allow_blank=True, max_length=200)
    description  = serializers.CharField(required=False, allow_blank=True, max_length=200)
    creator      = serializers.CharField(required=False, allow_blank=True, max_length=200)
    #creation_date = serializers.DateTimeField(auto_now_add=True)
    file_format   = serializers.CharField(required=False, allow_blank=True, max_length=200)
    #email = serializers.EmailField()
"""
#############################################################

class ChicModelSerializer(serializers.ModelSerializer):
    """
    Class creating serialisation without replicating the Model fields in the serializer manually
    """
    class Meta:
        model = ChicModel
        # fields = ()
    def create(self, validated_data):
        """
        Create and return a new `ChicModel` instance, given the validated data.
        """
        return ChicModel.objects.create(**validated_data)

    def update(self, instance, validated_data):
        """
        Update and return an existing `ChicModel` instance, given the validated data.
        """
        instance.model_unique_name = validated_data.get('model_unique_name', instance.model_unique_name)
        instance.model_url = validated_data.get('model_unique_name', instance.model_unique_name)
        instance.save()
        return instance


