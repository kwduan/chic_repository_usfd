__author__ = 'Daniele Tartarini'

from django.db import models
import datetime
from django.utils import timezone

from pygments.lexers import get_all_lexers
from pygments.styles import get_all_styles

################################################################################
LEXERS = [item for item in get_all_lexers() if item[1]]
LANGUAGE_CHOICES = sorted([(item[1][0], item[0]) for item in LEXERS])
STYLE_CHOICES = sorted((item, item) for item in get_all_styles())


################################################################################
class ChicModel(models.Model):
    model_id         = models.AutoField(primary_key=True)
    model_unique_name= models.CharField(max_length=100)     # change name
    model_url        = models.CharField(max_length=300)
    publication_date = models.DateField(blank=True) # change name
    #publication_date = models.DateField(auto_now=True) # For automatical filling of date time.
    modeller_name    = models.CharField(max_length=200, blank=True)
    institution      = models.CharField(max_length=200, blank=True)
    title            = models.CharField(max_length=200, blank=True)
    description      = models.CharField(max_length=500, blank=True)
    creator          = models.CharField(max_length=200, blank=True)
    creation_date    = models.DateTimeField(auto_now_add=True)
    file_format      = models.CharField(max_length=100, blank=True, default='T2flow')

    def Meta(self):
        unique_together = ("model_id", "model_unique_name")
    def __str__(self):              # __unicode__ on Python 2
        return self.model_unique_name




################################################################################
# class ChicModeller(models.Model):
#     mName = models.CharField(max_length=200)
#    def __str__(self):              # __unicode__ on Python 2
#        return self.mName

################################################################################

#class ChicModellerHasModel(models.Model):
#    FKModel = models.ForeignKey(ChicModel)
#    FKModeller = models.ForeignKey(ChicModeller)





