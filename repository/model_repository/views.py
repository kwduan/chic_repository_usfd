__author__ = 'Kewei Duan, Daniele Tartarini'

from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt

from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser
# from rest_framework.renderers import XMLRenderer
# from rest_framework.parsers import XMLParser

from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response

import urllib.request
import base64



#################################################
#

from .models import ChicModel
from .serializers import ChicModelSerializer
from .model_settings import sms_list, username, password


class JSONResponse(HttpResponse):
    """
    An HttpResponse that renders its content into JSON.
    """

    def __init__(self, data, **kwargs):
        content = JSONRenderer().render(data)
        kwargs['content_type'] = 'application/json'
        super(JSONResponse, self).__init__(content, **kwargs)


@api_view(['GET'])
def models_list(request):
    """
    List all ChicModels
    """
    if request.method == 'GET':
        models = ChicModel.objects.all()
        serializer = ChicModelSerializer(models, many=True)
        return JSONResponse(serializer.data)


"""
    elif request.method == 'POST':
        data = JSONParser().parse(request)
        serializer = ChicModelSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return JSONResponse(serializer.data, status=201)
        return JSONResponse(serializer.errors, status=400)
"""


@api_view(['GET', 'PUT', 'DELETE', 'POST'])
def model_detail(request, model_unique_name_par):
    """
    Retrieve, update or delete a Model instance.
    TODO: validate PUT
    """

    try:
        current_model = ChicModel.objects.get(
            model_unique_name=model_unique_name_par)
    except ChicModel.DoesNotExist:
        """
        if request.method == 'PUT':
            new_model = ChicModel.objects.get(
                 model_unique_name=model_unique_name_par)
            data = JSONParser().parse(request)
            #serializer = ChicModelSerializer(new_model, data=request.data)
            #serializer = ChicModelSerializer(new_model, data=data)
            if serializer.is_valid():
                serializer.save()
                #return Response(serializer.data)
                return JSONResponse(serializer.data)
            return Response(serializer.errors, 
                    status=status.HTTP_400_BAD_REQUEST)
        return Response(status=status.HTTP_404_NOT_FOUND)
        """
        if request.method == 'POST':
            new_data = JSONParser().parse(request)
            json_serializer = ChicModelSerializer(data=new_data)
            if json_serializer.is_valid():
                json_serializer.save()
                return Response(status=status.HTTP_201_CREATED)
            return Response(json_serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = ChicModelSerializer(current_model)
        # return Response(serializer.data)
        return JSONResponse(serializer.data)
        # return XMLRenderer.render(serializer.data)

    elif request.method == 'DELETE':
        current_model.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

    elif request.method == 'PUT':
        serializer = ChicModelSerializer(current_model, data=request.data)
        # When request.data is accessed, default corresponding parser will be called automatically
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET', 'PUT', 'DELETE'])
def get_model_url(request, model_unique_name_par):
    """
    Retrieve, update or delete a model URL.
    """
    try:
        model = ChicModel.objects.get(model_unique_name=model_unique_name_par)
    except model.DoesNotExist:
        return HttpResponse(status=404)

    if request.method == 'GET':
        # serializer = ChicModelSerializer(model)
        return JSONResponse(model.model_url)

    elif request.method == 'PUT':
        data = JSONParser().parse(request)
        model.model_url = data
        model.save(update_fields=['model_url'])
        return Response(status.HTTP_200_OK)

    elif request.method == 'DELETE':
        model.delete()
        return HttpResponse(status=status.HTTP_204_NO_CONTENT)


@csrf_exempt
@api_view(['GET', 'PUT'])
def model_file(request, model_unique_name_par):
    """
    Retrieve, create, update a model file.
    """
    try:
        model = ChicModel.objects.get(model_unique_name=model_unique_name_par)
    except model.DoesNotExist:
        return HttpResponse(status=404)

    global is_sms, top_level_uri
    is_sms = False
    top_level_uri = ''
    uri_temp = model.model_url;
    for str_temp in sms_list:
        if uri_temp.startswith(str_temp):
            is_sms = True
            top_level_uri = str_temp

    if request.method == 'GET':
        if is_sms:
            request_temp = urllib.request.Request(model.model_url, method='GET')
            base64string = base64.b64encode(('%s:%s' % (username, password)).encode('utf-8'))
            base64string = base64string.decode('utf-8')
            request_temp.add_header("Authorization", "Basic %s" % base64string)
            data = urllib.request.urlopen(request_temp)
        else:
            data = urllib.request.urlopen(model.model_url)

        if data.getcode() == 200:
            return HttpResponse(data.read(), content_type='application/octet-stream')
        else:
            return HttpResponse(status=data.getcode())

    elif request.method == 'PUT':
        # There is an assumption that the destination URL allows updating.
        if request.content_type != 'application/octet-stream':
            return HttpResponse(status=status.HTTP_415_UNSUPPORTED_MEDIA_TYPE)
        else:
            body_temp = request.body
            if is_sms:
                request_temp = urllib.request.Request(model.model_url, method='PUT')
                base64string = base64.b64encode(('%s:%s' % (username, password)).encode('utf-8'))
                base64string = base64string.decode('utf-8')
                request_temp.add_header("Authorization", "Basic %s" % base64string)
                request_temp.add_header('Content-Type', 'application/octet-stream')
                req_result = urllib.request.urlopen(request_temp, data=body_temp)
            else:
                req_result = urllib.request.urlopen(model.model_url, body_temp,
                                                    {'Content-Type': 'application/octet-stream'})
        return HttpResponse(status=req_result.code)



