__author__ = 'Daniele Tartarini'

import unittest
from django.test import TestCase
from django.test import Client

class SimpleTest(unittest.TestCase):
    def setUp(self):
        # Every test needs a client.
        self.client = Client()

    def test_details(self):
        # Issue a GET request.
        response = self.client.get('/repository/')

        # Check that the response is 200 OK.
        self.assertEqual(response.status_code, 200)

        # Check that the rendered context contains 5 customers.
       # self.assertEqual(len(response.context['customers']), 5)# Create your tests here.


    def test_det_model_details(self):
        # Issue a GET request.
        response = self.client.get('/repository/model/1/')

        # Check that the response is 200 OK.
        self.assertEqual(response.status_code, 200)