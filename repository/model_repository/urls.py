__author__ = 'Daniele Tartarini'

from django.conf.urls import patterns, url

from . import views

urlpatterns = patterns('',
    # url(r'^$', views.index, name='index'),
    # get all models: /models/
    url(r'^models/$', views.models_list, name='models_list'),

    # /model/<model_unique_name_par>
    url(r'^model/(?P<model_unique_name_par>\w+)/$', views.model_detail, 
        name='results'),

    # get model url: /model/<model_unique_name_par>/url
    url(r'^model/(?P<model_unique_name_par>\w+)/url/$', views.get_model_url, 
        name='results'),

    # model file: /model_file/<model_unique_name_par>
    url(r'^model_file/(?P<model_unique_name_par>\w+)/$', views.model_file,
        name='results'),
   
    # ex: /model/5/
    # url(r'^/model/(?P<model_id>)/$', views.models_list),#views.model_detail, 
    #    name='model_detail'),
    
    # url(r'^model/(?P<model_id>\d+)/$', views.model_detail, name='results'),

    #url(r'^/model/(?P<model_id>)/$', views.models_list),#views.model_detail, 
    #    name='model_detail'),
    

    # ex: /model/5/description/
    # url(r'^model/(?P<pk>\d+)/url/$',views.get_model_url,name='get_model_url'),

    # ex: /model/5/description/
    # url(r'^model/(?P<model_id>\d+)/url$', views.results, name='results'),

    # ex: /model/5/metadata/
    #url(r'^(?P<model_id>\d+)/metadata/$', views.metadata, name='metadata'),

    # ex: /model/5/results/
    # url(r'^model/(?P<model_id>\d+)/results/$', views.results, name='results'),
        
)
